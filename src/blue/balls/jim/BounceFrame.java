/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blue.balls.jim;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.Random;

/**
 * Shows an animated bouncing ball.
 * @version 1.34 2015-06-21
 * @author Cay Horstmann
 */

/**
 * The frame with ball component and buttons.
 */
class BounceFrame extends JFrame
{
   private PersonComponent comp;
   public static final int STEPS = 1000;
   public static final int DELAY = 3;

   /**
    * Constructs the frame with the component for showing the bouncing ball and
    * Start and Close buttons
    */
   public BounceFrame()
   {
      setTitle("Bounce");
      comp = new PersonComponent();
      add(comp, BorderLayout.CENTER);
      JPanel buttonPanel = new JPanel();
      addButton(buttonPanel, "Start", event -> addBall());
      addButton(buttonPanel, "Close", event -> System.exit(0));
      add(buttonPanel, BorderLayout.SOUTH);
      pack();
   }

   /**
    * Adds a button to a container.
    * @param c the container
    * @param title the button title
    * @param listener the action listener for the button
    */
   public void addButton(Container c, String title, ActionListener listener)
   {
      JButton button = new JButton(title);
      c.add(button);
      button.addActionListener(listener);
   }
    
   /**
    * Generates a random number between 1 (inclusive) and 7 (exclusive) to be used for a Persons speed/fitness stats
    */
   public int speedFitnessGenerator()
    {
        Random r = new Random();
        int low = 1;
        int high = 7;
        int result = r.nextInt(high-low) + low;
        return result;
    }   
   /**
    * Adds a bouncing ball to the panel and makes it bounce 1,000 times.
    */
   public void addBall()
   {

    try
    {
        Person person = new Person(speedFitnessGenerator(), speedFitnessGenerator());
        comp.add(person);

        for (int i = 1; i <= STEPS; i++)
        {
           person.move(comp.getBounds());
           comp.paint(comp.getGraphics());
           Thread.sleep(DELAY);
        }
    }
    catch (InterruptedException e)
    {
    }
   }
}
