/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blue.balls.jim;
import java.awt.geom.*;
import java.awt.Image;
import java.awt.Toolkit;

/**
 * A ball that moves and bounces off the edges of a rectangle
 * @version 1.33 2007-05-17
 * @author Cay Horstmann
 */
class Person
{
   private static final int XSIZE = 15;
   private static final int YSIZE = 15;
   private double x = 0;
   private double y = 0;
   private double dx = 1;
   private double dy = 1;
   
   private int speed;
   private int fitness;
   private Image baseImage;
   private int direction;
   private Image armsImage;
   private Image legsImage;
   private Image chestImage;
   private Image backImage;
   private Image shouldersImage;
   private boolean armsTrue = false;
   private boolean legsTrue = false;
   private boolean chestTrue = false;
   private boolean backTrue = false;
   private boolean shouldersTrue = false;
   
   
   /**
    * Constructor for creating a new person
    * @param newSex
    * @param newSpeed
    * @param newFitness 
    * @author Coby Martin
    */
   public Person(int newSpeed, int newFitness)
   {
       speed = newSpeed;
       fitness = newFitness;
       baseImage = Toolkit.getDefaultToolkit().getImage("build/images/person/baseImage.png");
       armsImage = Toolkit.getDefaultToolkit().getImage("build/images/person/arms.png");
       legsImage = Toolkit.getDefaultToolkit().getImage("build/images/person/legs.png");
       chestImage = Toolkit.getDefaultToolkit().getImage("build/images/person/chest.png");
       backImage = Toolkit.getDefaultToolkit().getImage("build/images/person/back.png");
       shouldersImage = Toolkit.getDefaultToolkit().getImage("build/images/person/shoulders.png");
   }
   

   /**
    * Moves the ball to the next position, reversing direction if it hits one of the edges
    */
   public void move(Rectangle2D bounds)
   {
      x += dx;
      y += dy;
      if (x < bounds.getMinX())
      {
         x = bounds.getMinX();
         dx = -dx;
      }
      if (x + XSIZE >= bounds.getMaxX())
      {
         x = bounds.getMaxX() - XSIZE;
         dx = -dx;
      }
      if (y < bounds.getMinY())
      {
         y = bounds.getMinY();
         dy = -dy;
      }
      if (y + YSIZE >= bounds.getMaxY())
      {
         y = bounds.getMaxY() - YSIZE;
         dy = -dy;
      }
   }

   /**
    * Gets the shape of the ball at its current position.
    */
    public Image getBaseImage()
    {
        return baseImage;
    }
    public Image getArmsImage()
    {
        return armsImage;
    }
    public Image getLegsImage()
    {
       return legsImage;
    }
    public Image getChestImage()
    {
        return chestImage;
    }
    public Image getBackImage()
    {
       return backImage;
    }
    public Image getShouldersImage()
    {
       return shouldersImage;
    }
   
    public boolean checkForBoundary()
    {
        return true;
    }
   
    public void moveForwards()
    {

    }

    public void moveToQueue()
    {

    }

    public void moveUpQueue()
    {

    }

    public void getInPosition()
    {

    }

    public void exitMachine()
    {

    }

    public int getSpeed()
    {
        return speed;
    }

    public int getFitness()
    {
        return fitness;
    }

    public void setSpeed(int newSpeed)
    {
        speed = newSpeed;
    }

    public void setFitness(int newFitness)
    {
        fitness = newFitness;
    }

    public boolean getArms()
    {
        return armsTrue;
    }

    public void growArms()
    {
        armsTrue = true;
    }

    public boolean getLegs()
    {
        return legsTrue;
    }

    public void growLegs()
    {
        legsTrue = true;
    }

    public boolean getChest()
    {
        return chestTrue; 
    }

    public void growChest()
    {
        chestTrue = true;
    }

    public boolean getBack()
    {
        return backTrue;
    }

    public void growBack()
    {
        backTrue = true;
    }

    public boolean getShoulders()
    {
        return shouldersTrue;
    }

    public void growShoulders()
    {
        shouldersTrue = true;
    }
}
