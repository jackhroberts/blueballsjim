/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blue.balls.jim;
import java.awt.*;
import java.util.*;
import javax.swing.*;
import java.awt.Image;

/**
 * The component that draws the balls.
 * @version 1.34 2012-01-26
 * @author Cay Horstmann
 */
class PersonComponent extends JPanel
{
   private static final int DEFAULT_WIDTH = 450;
   private static final int DEFAULT_HEIGHT = 350;

   private java.util.List<Person> persons = new ArrayList<>();

   /**
    * Add a ball to the component.
    * @param b the ball to add
    */
   public void add(Person p)
   {
      persons.add(p);
   }

   public void paintComponent(Image[] images)
   {
      super.paintComponent(g); // erase background
      Graphics2D g2 = (Graphics2D) g;
      for (Person p : persons)
      {
        g2.drawImage(p.getBaseImage(), this);
        if (p.getArms())
        {
            g2.drawImage(p.getArmsImage());
        }
        if (p.getLegs())
        {
            g2.drawImage(p.getLegsImage());
        }
        if (p.getChest())
        {
            g2.drawImage(p.getChestImage());
        }
        if (p.getBack())
        {
            g2.drawImage(p.getBackImage());
        }
        if (p.getShoulders())
        {
            g2.drawImage(p.getShouldersImage());
        }
      }
   }
   
   public Dimension getPreferredSize() { return new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT); }
}
